/*
 * SmartHealth
 *
 * SmartHealth PMS API
 *
 * The version of the OpenAPI document: 6.1.111
 * 
 * Generated by: https://openapi-generator.tech
 */

using System;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Org.OpenAPITools.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class BookingEntry : IEquatable<BookingEntry>
    {
        /// <summary>
        /// The id of the profile.
        /// </summary>
        /// <value>The id of the profile.</value>
        [DataMember(Name="ProfileId", EmitDefaultValue=false)]
        public int ProfileId { get; set; }

        /// <summary>
        /// It is possible to signal that the patient want to be called back as soon as possible. This option is only available if there is no agent selected (Slot.AgentId &#x3D; null) (currently only for german slot). Otherwise this property will be ignored.
        /// </summary>
        /// <value>It is possible to signal that the patient want to be called back as soon as possible. This option is only available if there is no agent selected (Slot.AgentId &#x3D; null) (currently only for german slot). Otherwise this property will be ignored.</value>
        [DataMember(Name="CallAsap", EmitDefaultValue=false)]
        public bool CallAsap { get; set; }

        /// <summary>
        /// Gets or Sets Slot
        /// </summary>
        [DataMember(Name="Slot", EmitDefaultValue=false)]
        public Slot Slot { get; set; }

        /// <summary>
        /// indicates that a referral is required for the entry being booked.
        /// </summary>
        /// <value>indicates that a referral is required for the entry being booked.</value>
        [DataMember(Name="ReferralRequired", EmitDefaultValue=false)]
        public bool ReferralRequired { get; set; }

        /// <summary>
        /// indicates that a work incapacity is required for the entry being booked.
        /// </summary>
        /// <value>indicates that a work incapacity is required for the entry being booked.</value>
        [DataMember(Name="WorkIncapacityRequired", EmitDefaultValue=false)]
        public bool WorkIncapacityRequired { get; set; }

        /// <summary>
        /// indicates that a prescription is required for the entry being booked.
        /// </summary>
        /// <value>indicates that a prescription is required for the entry being booked.</value>
        [DataMember(Name="PrescriptionRequired", EmitDefaultValue=false)]
        public bool PrescriptionRequired { get; set; }

        /// <summary>
        /// Phonenumber from account.
        /// </summary>
        /// <value>Phonenumber from account.</value>
        [DataMember(Name="PhoneNumber", EmitDefaultValue=false)]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// indicates that a video call is requested by the patient.
        /// </summary>
        /// <value>indicates that a video call is requested by the patient.</value>
        [DataMember(Name="IsVideoRequested", EmitDefaultValue=false)]
        public bool IsVideoRequested { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class BookingEntry {\n");
            sb.Append("  ProfileId: ").Append(ProfileId).Append("\n");
            sb.Append("  CallAsap: ").Append(CallAsap).Append("\n");
            sb.Append("  Slot: ").Append(Slot).Append("\n");
            sb.Append("  ReferralRequired: ").Append(ReferralRequired).Append("\n");
            sb.Append("  WorkIncapacityRequired: ").Append(WorkIncapacityRequired).Append("\n");
            sb.Append("  PrescriptionRequired: ").Append(PrescriptionRequired).Append("\n");
            sb.Append("  PhoneNumber: ").Append(PhoneNumber).Append("\n");
            sb.Append("  IsVideoRequested: ").Append(IsVideoRequested).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((BookingEntry)obj);
        }

        /// <summary>
        /// Returns true if BookingEntry instances are equal
        /// </summary>
        /// <param name="other">Instance of BookingEntry to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(BookingEntry other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    ProfileId == other.ProfileId ||
                    
                    ProfileId.Equals(other.ProfileId)
                ) && 
                (
                    CallAsap == other.CallAsap ||
                    
                    CallAsap.Equals(other.CallAsap)
                ) && 
                (
                    Slot == other.Slot ||
                    Slot != null &&
                    Slot.Equals(other.Slot)
                ) && 
                (
                    ReferralRequired == other.ReferralRequired ||
                    
                    ReferralRequired.Equals(other.ReferralRequired)
                ) && 
                (
                    WorkIncapacityRequired == other.WorkIncapacityRequired ||
                    
                    WorkIncapacityRequired.Equals(other.WorkIncapacityRequired)
                ) && 
                (
                    PrescriptionRequired == other.PrescriptionRequired ||
                    
                    PrescriptionRequired.Equals(other.PrescriptionRequired)
                ) && 
                (
                    PhoneNumber == other.PhoneNumber ||
                    PhoneNumber != null &&
                    PhoneNumber.Equals(other.PhoneNumber)
                ) && 
                (
                    IsVideoRequested == other.IsVideoRequested ||
                    
                    IsVideoRequested.Equals(other.IsVideoRequested)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    
                    hashCode = hashCode * 59 + ProfileId.GetHashCode();
                    
                    hashCode = hashCode * 59 + CallAsap.GetHashCode();
                    if (Slot != null)
                    hashCode = hashCode * 59 + Slot.GetHashCode();
                    
                    hashCode = hashCode * 59 + ReferralRequired.GetHashCode();
                    
                    hashCode = hashCode * 59 + WorkIncapacityRequired.GetHashCode();
                    
                    hashCode = hashCode * 59 + PrescriptionRequired.GetHashCode();
                    if (PhoneNumber != null)
                    hashCode = hashCode * 59 + PhoneNumber.GetHashCode();
                    
                    hashCode = hashCode * 59 + IsVideoRequested.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(BookingEntry left, BookingEntry right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(BookingEntry left, BookingEntry right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
