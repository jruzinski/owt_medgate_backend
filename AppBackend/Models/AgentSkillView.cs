/*
 * Back-end REST API
 *
 * Implemented REST endpoints on the back-end application
 *
 * The version of the OpenAPI document: 1.0
 * 
 * Generated by: https://openapi-generator.tech
 */

using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace AppBackend.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class AgentSkillView : IEquatable<AgentSkillView>
    {
        /// <summary>
        /// Gets or Sets DoctorId
        /// </summary>
        [DataMember(Name="doctorId", EmitDefaultValue=false)]
        public long DoctorId { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class AgentSkillView {\n");
            sb.Append("  DoctorId: ").Append(DoctorId).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((AgentSkillView)obj);
        }

        /// <summary>
        /// Returns true if AgentSkillView instances are equal
        /// </summary>
        /// <param name="other">Instance of AgentSkillView to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(AgentSkillView other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    DoctorId == other.DoctorId ||
                    
                    DoctorId.Equals(other.DoctorId)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    
                    hashCode = hashCode * 59 + DoctorId.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(AgentSkillView left, AgentSkillView right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(AgentSkillView left, AgentSkillView right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
