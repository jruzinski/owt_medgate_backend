using System.Globalization;
using AppBackend.Attributes;
using Microsoft.AspNetCore.Http.Features;

namespace AppBackend;

public class PmsAliveMiddleware
{
    private readonly RequestDelegate _next;
    
    public PmsAliveMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context, PmsService pmsService)
    {
        var endpoint = context.Features.Get<IEndpointFeature>()?.Endpoint;
        var attribute = endpoint?.Metadata.GetMetadata<PmsActiveCheckAttribute>();

        if (attribute is not null)
        {
            await _next(context);
            return;
        }


        if (!await pmsService.IsActiveAsync())
        {
            context.Response.StatusCode = 503;
            return;
        }

        await _next(context);
    }
}