using System.Net;
using AppBackend.Models;
using Newtonsoft.Json.Linq;

namespace AppBackend;

public class PmsService
{
    private HttpClient Client { get; }
    
    public PmsService()
    {
        Client = new()
        {
            BaseAddress = new ("localhost:8778")
        };
    }

    public async Task<HttpStatusCode> CreateAccountAsync(CreateAccountRestRequest request)
    {
        Org.OpenAPITools.Models.Account account = new()
        {
            Email = request.Email,
            PasswordHash = request.Password,
            GivenName = request.FirstName,
            FamilyName = request.LastName,
            Gender = request.Gender
        };

        var accountJson = JObject.FromObject(account);
        accountJson.Add("Language", new JValue(request.Language));
        StringContent content = new(accountJson.ToString());
        var response = await Client.PostAsync("/v1/accounts", content).ConfigureAwait(false);
        return response.StatusCode;
    }

    public async Task<bool> IsActiveAsync() {
        var response = await Client.GetAsync("/smarthealth/v1/alive").ConfigureAwait(false);
        if (response.IsSuccessStatusCode)
            return await response.Content.ReadFromJsonAsync<bool>();
        
        return false;
    } 
}