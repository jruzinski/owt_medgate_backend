using System.ComponentModel;
using System.Runtime.Serialization;
using AppBackend.Converters;
using Newtonsoft.Json;

namespace AppBackend.Enums;

/// <summary>
/// Gets or Sets Language
/// </summary>
[TypeConverter(typeof(CustomEnumConverter<LanguageEnum>))]
[JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
public enum LanguageEnum
{
        
    /// <summary>
    /// Enum DeEnum for de
    /// </summary>
    [EnumMember(Value = "de")]
    DeEnum = 1,
        
    /// <summary>
    /// Enum FrEnum for fr
    /// </summary>
    [EnumMember(Value = "fr")]
    FrEnum = 2,
        
    /// <summary>
    /// Enum ItEnum for it
    /// </summary>
    [EnumMember(Value = "it")]
    ItEnum = 3,
        
    /// <summary>
    /// Enum EnEnum for en
    /// </summary>
    [EnumMember(Value = "en")]
    EnEnum = 4
}
