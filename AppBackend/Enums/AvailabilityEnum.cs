using System.ComponentModel;
using System.Runtime.Serialization;
using AppBackend.Converters;
using Newtonsoft.Json;

namespace AppBackend.Enums;

/// <summary>
/// Gets or Sets Availability
/// </summary>
[TypeConverter(typeof(CustomEnumConverter<AvailabilityEnum>))]
[JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
public enum AvailabilityEnum
{
            
    /// <summary>
    /// Enum NormalEnum for normal
    /// </summary>
    [EnumMember(Value = "normal")]
    NormalEnum = 1,
            
    /// <summary>
    /// Enum LowEnum for low
    /// </summary>
    [EnumMember(Value = "low")]
    LowEnum = 2,
            
    /// <summary>
    /// Enum VeryLowEnum for veryLow
    /// </summary>
    [EnumMember(Value = "veryLow")]
    VeryLowEnum = 3
}