using System.ComponentModel;
using System.Runtime.Serialization;
using AppBackend.Converters;
using Newtonsoft.Json;

namespace AppBackend.Enums;

/// <summary>
/// Gets or Sets Status
/// </summary>
[TypeConverter(typeof(CustomEnumConverter<StatusEnum>))]
[JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
public enum StatusEnum
{
        
    /// <summary>
    /// Enum RegisteredEnum for registered
    /// </summary>
    [EnumMember(Value = "registered")]
    RegisteredEnum = 1,
        
    /// <summary>
    /// Enum IdentifiedEnum for identified
    /// </summary>
    [EnumMember(Value = "identified")]
    IdentifiedEnum = 2
}