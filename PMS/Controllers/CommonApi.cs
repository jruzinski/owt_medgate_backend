/*
 * SmartHealth
 *
 * SmartHealth PMS API
 *
 * The version of the OpenAPI document: 6.1.111
 * 
 * Generated by: https://openapi-generator.tech
 */

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.SwaggerGen;
using Newtonsoft.Json;
using Org.OpenAPITools.Attributes;
using Org.OpenAPITools.Models;

namespace Org.OpenAPITools.Controllers
{ 
    /// <summary>
    /// 
    /// </summary>
    [ApiController]
    public class CommonApiController : ControllerBase
    { 
        /// <summary>
        /// Alive check.
        /// </summary>
        /// <remarks>Checks if the service is alive.</remarks>
        /// <response code="200">True if everything is okay.</response>
        /// <response code="0">Error message</response>
        [HttpGet]
        [Route("/smarthealth/v1/alive")]
        [ValidateModelState]
        [SwaggerOperation("V1AliveGet")]
        [SwaggerResponse(statusCode: 200, type: typeof(bool), description: "True if everything is okay.")]
        [SwaggerResponse(statusCode: 0, type: typeof(string), description: "Error message")]
        public virtual IActionResult V1AliveGet()
        {
            return new ObjectResult("true");
        }
    }
}
